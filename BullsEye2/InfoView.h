//
//  InfoView.h
//  BullsEye2
//
//  Created by Valerun on 05.05.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoView : UIView

@property (nonatomic, strong) UIButton *closeButton;

@end
