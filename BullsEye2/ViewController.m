//
//  ViewController.m
//  BullsEye2
//
//  Created by Valerun on 09.04.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "ViewController.h"
#import "BullsEyeView.h"
#import "AboutViewController.h"

@interface ViewController ()

@property (nonatomic) BullsEyeView *view;

@end

@implementation ViewController {
    int _currentValue;
    int _targetValue;
    int _score;
    int _round;
}

@dynamic view;

- (void)loadView {
    self.view = [BullsEyeView new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.view.hitMeButton addTarget:self
                              action:@selector(hitMe)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [self.view.slider addTarget:self
                         action:@selector(sliderMoved:)
               forControlEvents:UIControlEventValueChanged];
    
    [self.view.startNewGame addTarget:self
                               action:@selector(startOver)
                     forControlEvents:UIControlEventTouchUpInside];
    
    [self.view.infoButton addTarget:self
                             action:@selector(infoButtonPressed)
                   forControlEvents:UIControlEventTouchUpInside];
    
    [self.view setUserInteractionEnabled:YES];
    
    [self starNewGame];
    [self updateLabels];
    
    [self.view setNeedsLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sliderMoved:(UISlider *)sender
{
    _currentValue = (int)lroundf(sender.value);
}

- (void)hitMe {
    int difference = abs(_targetValue - _currentValue);
    int points = 100 - difference;
    
    NSString *title;
    
    if (difference == 0) {
        title = @"Perfect!";
        points += 100;
    } else if (difference < 5) {
        title = @"You almost had it!";
        if (difference == 1) {
            points += 50;
        }
    } else if (difference < 10) {
        title = @"Pretty good!";
    } else {
        title = @"Not even close...";
    }
    
    _score += points;
    
    NSString *massage = [NSString stringWithFormat:@"You scored %d points!", points];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:massage
                                                       delegate:self
                                              cancelButtonTitle:@"OK!"
                                              otherButtonTitles:nil];
    
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self startNewRound];
    [self updateLabels];
}

- (void)startNewRound
{
    _round += 1;
    
    _targetValue = 1 + arc4random_uniform(100);
    
    _currentValue = 50;
    self.view.slider.value = _currentValue;
}

- (void)starNewGame
{
    _score = 0;
    _round = 0;
    [self startNewRound];
}

- (void)updateLabels
{
    self.view.needValueLabel.text = [NSString stringWithFormat:@"%d", _targetValue];
    self.view.score.text = [NSString stringWithFormat:@"%d", _score];
    self.view.round.text = [NSString stringWithFormat:@"%d", _round];
}

- (void)startOver
{
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade;
    transition.duration = 1;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    [self starNewGame];
    [self updateLabels];
    
    [self.view.layer addAnimation:transition forKey:nil];
}

- (void)infoButtonPressed
{
    NSLog(@"Zalupa");
    AboutViewController * controller = [AboutViewController new];
    
    controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:controller animated:YES completion:nil];
}

@end
