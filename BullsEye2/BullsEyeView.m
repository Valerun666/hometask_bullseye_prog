//
//  BullsEyeView.m
//  BullsEye2
//
//  Created by Valerun on 09.04.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "BullsEyeView.h"
#import "UIImage+Resize.h"

@implementation BullsEyeView

#pragma TODO: add methods to each button, add constants -> fonts


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.imageView = [UIImageView new];
        [self addSubview:self.imageView];
        
        // HeaderLabel
        self.headerLabel = [UILabel new];
        self.headerLabel.numberOfLines = 1;
        self.headerLabel.text = NSLocalizedString(@"Put the Bull's Eye as close as you can:", @"Bull'sEye View: HeaderLabel");
        self.headerLabel.textColor = [UIColor whiteColor];
        self.headerLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:16.0];
        [self addSubview:self.headerLabel];

        //NeededValueLabel
        self.needValueLabel = [UILabel new];
        self.needValueLabel.text = @"100";
        self.needValueLabel.textColor = [UIColor whiteColor];
        self.needValueLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:20.0];
        [self addSubview:self.needValueLabel];

        //MinValueLabel
        self.minValueLabel = [UILabel new];
        self.minValueLabel.text = @"1";
        self.minValueLabel.textColor = [UIColor whiteColor];
        self.minValueLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:14];
        [self addSubview:self.minValueLabel];

        //MaxValueLabel
        self.maxValueLabel = [UILabel new];
        self.maxValueLabel.text = @"100";
        self.maxValueLabel.textColor = [UIColor whiteColor];
        self.maxValueLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:14];
        [self addSubview:self.maxValueLabel];
        
//      Slider
        self.slider = [UISlider new];
        self.slider.minimumValue = 1.0;
        self.slider.maximumValue = 100.0;
        [self.slider setValue:50.0];
        self.slider.continuous = NO;
        [self.slider setUserInteractionEnabled:YES];
        [self setupSliderAppearance];
        
        [self addSubview:self.slider];
        
//      HitMeButton
        self.hitMeButton = [UIButton new];
        
        UIColor *colorForHitMeButton = [UIColor colorWithRed:96 / 255.0 green:30 / 255.0 blue:0 / 255.0 alpha:1];
        
        [self.hitMeButton.titleLabel setFont:[UIFont fontWithName:@"Arial Rounded MT Bold" size:20]];
        [self.hitMeButton setTitleColor:colorForHitMeButton forState:UIControlStateNormal];
        [self.hitMeButton setTitle:@"Hit Me!" forState:UIControlStateNormal];
        self.hitMeButton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Button-Normal"]];
        self.hitMeButton.layer.cornerRadius = 5.0;
        self.hitMeButton.layer.borderWidth = 2.0;
        self.hitMeButton.layer.borderColor = [colorForHitMeButton CGColor];
        [self addSubview:self.hitMeButton];

//      ScoreLabel
        self.scoreLabel = [UILabel new];
        self.scoreLabel.text = NSLocalizedString(@"Score:", @"Bull'sEye View: ScoreLabel");
        self.scoreLabel.textColor = [UIColor whiteColor];
        [self.scoreLabel setFont:[UIFont fontWithName:@"Arial Rounded MT Bold" size:16]];
        [self.scoreLabel sizeToFit];
        [self addSubview:self.scoreLabel];

//      Score
        self.score = [UILabel new];
        self.score.text = @"99999";
        self.score.textColor = [UIColor whiteColor];
        [self.score setFont:[UIFont fontWithName:@"Arial Rounded MT Bold" size:20]];
        [self.score sizeToFit];
        [self addSubview:self.score];

//      RoundLabel
        self.roundLabel = [UILabel new];
        self.roundLabel.text = NSLocalizedString(@"Round:", @"Bull'sEye View: RoundLabel");
        self.roundLabel.textColor = [UIColor whiteColor];
        [self.roundLabel setFont:[UIFont fontWithName:@"Arial Rounded MT Bold" size:16]];
        [self addSubview:self.roundLabel];

//      Round
        
        self.round = [UILabel new];
        self.round.text = @"999";
        self.round.textColor = [UIColor whiteColor];
        [self.round setFont:[UIFont fontWithName:@"Arial Rounded MT Bold" size:20]];
        [self addSubview:self.round];
        
//      InfoButton
         UIImage *smallButtonImage = [UIImage imageNamed:@"SmallButton"];
        
        self.infoButton = [UIButton new];
        self.infoButton.backgroundColor = [UIColor colorWithPatternImage:smallButtonImage];
        [self.infoButton setImage:[UIImage imageNamed:@"InfoButton"] forState:UIControlStateNormal];
        [self addSubview:self.infoButton];
        
//      RestartRoundButton
        self.startNewGame = [UIButton new];
        self.startNewGame.backgroundColor = [UIColor colorWithPatternImage:smallButtonImage];
        [self.startNewGame setImage:[UIImage imageNamed:@"StartOverIcon"] forState:UIControlStateNormal];
        [self addSubview:self.startNewGame];
    }
    
    return self;
}

- (void)setupSliderAppearance
{
    UIImage *sliderTrackLeft = [[UIImage imageNamed:@"SliderTrackLeft"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 14, 0, 14)];
    UIImage *sliderTrackRight = [[UIImage imageNamed:@"SliderTrackRight"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 14, 0, 14)];
    UIImage *sliderThumbNormal = [UIImage imageNamed:@"SliderThumb-Normal"];
    UIImage *sliderThumbHighlighted = [UIImage imageNamed:@"SliderThumb-Highlighted"];
    
    [self.slider setMaximumTrackImage:sliderTrackRight forState:UIControlStateNormal];
    [self.slider setMinimumTrackImage:sliderTrackLeft forState:UIControlStateNormal];
    [self.slider setThumbImage:sliderThumbNormal forState:UIControlStateNormal];
    [self.slider setThumbImage:sliderThumbHighlighted forState:UIControlStateHighlighted];
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat midX = self.center.x;
    CGFloat midY = self.center.y;
 
//  ImageView
    self.imageView.frame = self.bounds;
    self.imageView.image = [[UIImage imageNamed:@"Background"] resizeImageWithBounds:self.bounds.size];
    
//  HeaderLabel
    CGFloat headerLabelX = midX - (self.headerLabel.bounds.size.width + self.needValueLabel.frame.size.width) / 2.0;
    
    _headerLabel.frame = CGRectMake(headerLabelX,
                                    20,
                                    _headerLabel.intrinsicContentSize.width,
                                    _headerLabel.intrinsicContentSize.height);

//  NeedValueLabel
    _needValueLabel.frame = CGRectMake(CGRectGetMaxX(_headerLabel.frame) + 6,
                                           16,
                                           _needValueLabel.intrinsicContentSize.width,
                                           _needValueLabel.intrinsicContentSize.height);
  
//  Slider
    CGFloat sliderWidth = (midX - 100) * 2.0;
    CGFloat sliderX = (self.bounds.size.width - sliderWidth) / 2.0;
    
    _slider.frame = CGRectMake(sliderX,
                               CGRectGetMaxY(_headerLabel.frame) + 40,
                               sliderWidth,
                               30);
    
//  MinValueLabel
    _minValueLabel.frame = CGRectMake(_slider.frame.origin.x - _minValueLabel.intrinsicContentSize.width - 6,
                                      _slider.frame.origin.y + round(_minValueLabel.intrinsicContentSize.height / 2.0) - 1,
                                      _minValueLabel.intrinsicContentSize.width,
                                      _minValueLabel.intrinsicContentSize.height);
    
//  MaxValueLabel
    self.maxValueLabel.frame = CGRectMake(_slider.frame.origin.x + self.slider.bounds.size.width + 6,
                                          _minValueLabel.frame.origin.y,
                                          _maxValueLabel.intrinsicContentSize.width,
                                          _maxValueLabel.intrinsicContentSize.height);
    
//  HitMeButton
    CGSize hitMeButtonSize = CGSizeMake(100, 37);
    self.hitMeButton.frame = CGRectMake(midX - hitMeButtonSize.width / 2,
                                        midY - hitMeButtonSize.height / 2,
                                        hitMeButtonSize.width,
                                        hitMeButtonSize.height);
    
//  RestartRoundButton
    CGSize smallButtonSize = CGSizeMake(32, 32);
    
    _startNewGame.frame = CGRectMake(48,
                                           self.bounds.size.height - smallButtonSize.height - 20,
                                           smallButtonSize.width,
                                           smallButtonSize.height);

//  ScoreLabel
    _scoreLabel.frame = CGRectMake(CGRectGetMaxX(_startNewGame.frame) + 20,
                                       _startNewGame.frame.origin.y + _scoreLabel.intrinsicContentSize.height / 2.0,
                                       _scoreLabel.intrinsicContentSize.width,
                                       _scoreLabel.intrinsicContentSize.height);
    
    NSLog(@"%f, %f, %f, %f", self.scoreLabel.frame.origin.x, self.scoreLabel.frame.origin.y, self.scoreLabel.frame.size.width, self.scoreLabel.frame.size.height);

//  Score
    _score.frame = CGRectMake(CGRectGetMaxX(_scoreLabel.frame) + 6,
                                  _scoreLabel.frame.origin.y - 3,
                                  60,
                                  24);

//  InfoButton
    _infoButton.frame = CGRectMake(self.bounds.size.width - CGRectGetMaxX(_startNewGame.frame),
                                       _startNewGame.frame.origin.y,
                                       smallButtonSize.width,
                                       smallButtonSize.height);
    
//  Round
    _round.frame = CGRectMake(_infoButton.frame.origin.x - 20 - 50,
                                  _score.frame.origin.y,
                                  50,
                                  24);

//  RoundLabel
    _roundLabel.frame = CGRectMake(_round.frame.origin.x - 6 - _roundLabel.intrinsicContentSize.width,
                                   _scoreLabel.frame.origin.y,
                                   _roundLabel.intrinsicContentSize.width,
                                   _roundLabel.intrinsicContentSize.height);

}



@end
