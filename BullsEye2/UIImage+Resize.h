//
//  UIImage+Resize.h
//  BullsEye2
//
//  Created by Valerun on 04.05.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage(Resize)

-(UIImage *)resizeImageWithBounds:(CGSize)bounds;

@end
