//
//  InfoView.m
//  BullsEye2
//
//  Created by Valerun on 05.05.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "InfoView.h"
#import "UIImage+Resize.h"

@implementation InfoView {
    UIImageView *_imageView;
    UIWebView *_webView;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _imageView = [UIImageView new];
        [self addSubview:_imageView];
        
        _webView = [UIWebView new];
        
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"BullsEye" ofType:@"html"];
        NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];
        NSURL *baseURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]];
        
        [_webView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:baseURL];
        
        [self addSubview:_webView];
        
        self.closeButton = [UIButton new];
        UIColor *closeButtonColor = [UIColor colorWithRed:96 / 255.0 green:30 / 255.0 blue:0 / 255.0 alpha:1];
        self.closeButton.layer.borderWidth = 2.0;
        self.closeButton.layer.cornerRadius = 5.0;
        self.closeButton.layer.borderColor = [closeButtonColor CGColor];
        [self.closeButton.titleLabel setFont:[UIFont fontWithName:@"Arial Rounded MT Bold" size:20]];
        [self.closeButton setTitleColor:closeButtonColor
                               forState:UIControlStateNormal];
        [self.closeButton setTitle:@"Close" forState:UIControlStateNormal];
        self.closeButton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Button-Normal"]];
        
        [self addSubview:self.closeButton];
        
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _imageView.frame = self.bounds;
    _imageView.image = [[UIImage imageNamed:@"Background"] resizeImageWithBounds:self.bounds.size];
    
    _webView.frame = CGRectMake(20,
                                20,
                                self.bounds.size.width - 40,
                                self.bounds.size.height - 78);
    
    _closeButton.frame = CGRectMake(self.center.x - 50,
                                    CGRectGetMaxY(_webView.frame) + 10,
                                    100,
                                    37);
}


@end
