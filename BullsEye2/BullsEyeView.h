//
//  BullsEyeView.h
//  BullsEye2
//
//  Created by Valerun on 09.04.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BullsEyeView : UIView

@property (nonatomic, strong) UILabel *scoreLabel;
@property (nonatomic, strong) UILabel *score;
@property (nonatomic, strong) UILabel *roundLabel;
@property (nonatomic, strong) UILabel *round;
@property (nonatomic, strong) UILabel *minValueLabel;
@property (nonatomic, strong) UILabel *maxValueLabel;
@property (nonatomic, strong) UILabel *headerLabel;
@property (nonatomic, strong) UILabel *needValueLabel;
@property (nonatomic, strong) UIButton *startNewGame;
@property (nonatomic, strong) UIButton *infoButton;
@property (nonatomic, strong) UIButton *hitMeButton;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UIImageView *imageView;

@end
