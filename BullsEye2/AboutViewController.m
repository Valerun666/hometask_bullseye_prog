//
//  AboutViewController.m
//  BullsEye2
//
//  Created by Valerun on 05.05.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "AboutViewController.h"
#import "InfoView.h"

@interface AboutViewController ()

@property (nonatomic) InfoView *view;

@end

@implementation AboutViewController

@dynamic view;

- (void)loadView
{
    self.view = [InfoView new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view.closeButton addTarget:self
                              action:@selector(closeButtonPressed)
                    forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)closeButtonPressed
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
